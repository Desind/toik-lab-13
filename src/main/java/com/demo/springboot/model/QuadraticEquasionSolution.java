package com.demo.springboot.model;

import org.json.JSONException;
import org.json.JSONObject;

public class QuadraticEquasionSolution {
    Float x1;
    Float x2;

    public QuadraticEquasionSolution(Float x1, Float x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    public Float getX1() {
        return x1;
    }

    public Float getX2() {
        return x2;
    }

    @Override
    public String toString() {
        return "{" +
                "x1=" + x1 +
                ", x2=" + x2 +
                '}';
    }
}
