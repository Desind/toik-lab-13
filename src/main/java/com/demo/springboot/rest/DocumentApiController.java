package com.demo.springboot.rest;

import com.demo.springboot.model.QuadraticEquasionSolution;
import com.demo.springboot.service.QuadraticSolver;
import com.demo.springboot.service.QuadraticSolverImpl;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DocumentApiController {

    public DocumentApiController(){

    }

    QuadraticSolver qs = new QuadraticSolverImpl();

    @GetMapping
    @RequestMapping("/api/math/quadratic-function{urlid}")
    public ResponseEntity<Map<String,Float>> solveQuadraticEquasion(@RequestParam float a,@RequestParam float b, @RequestParam float c){
        QuadraticEquasionSolution quadraticEquasionSolution = qs.solveEquasion(a,b,c);
        Map<String, Float> equasionAnswer = new HashMap<>();
        equasionAnswer.put("x1", quadraticEquasionSolution.getX1());
        equasionAnswer.put("x2", quadraticEquasionSolution.getX2());
        return ResponseEntity.ok().body(equasionAnswer);
    }

}
