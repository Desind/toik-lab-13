package com.demo.springboot.service;

import com.demo.springboot.model.QuadraticEquasionSolution;

public class QuadraticSolverImpl implements QuadraticSolver{
    @Override
    public QuadraticEquasionSolution solveEquasion(float a, float b, float c) {
        Float x1;
        Float x2;

        Float delta = b*b - 4*a*c;
        if(delta<0){
            x1 = null;
            x2 = null;
        }else if(delta == 0){
            x1 = (-b)/2*a;
            x2 = null;
        }else{
            x1 = new Float(((-b)+Math.sqrt(delta))/2*a);
            x2 = new Float(((-b)-Math.sqrt(delta))/2*a);
        }
        System.out.println("delta: " + delta);
        System.out.println("x1: " + x1);
        System.out.println("x2: " + x2);
        return new QuadraticEquasionSolution(x1,x2);
    }
}
